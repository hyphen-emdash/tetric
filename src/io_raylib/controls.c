#include "internal.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include <raylib.h>

#define LEN(arr) sizeof(arr) / sizeof(arr[0])

ControlScheme control_dummy;

const char *input_names[] = {
    "move-left",
    "move-right",
    "hard-drop",
    "soft-drop",
    "rot-cw",
    "rot-ccw",
    "pause",
};

const ptrdiff_t control_mem_offsets[] = {
    (char *) &control_dummy.key_left - (char *) &control_dummy,
    (char *) &control_dummy.key_right - (char *) &control_dummy,
    (char *) &control_dummy.key_hard_drop - (char *) &control_dummy,
    (char *) &control_dummy.key_soft_drop - (char *) &control_dummy,
    (char *) &control_dummy.key_rot_clockwise - (char *) &control_dummy,
    (char *) &control_dummy.key_rot_widdershins - (char *) &control_dummy,
    (char *) &control_dummy.key_pause - (char *) &control_dummy,
};

static_assert(LEN(input_names) == LEN(control_mem_offsets), "Arrays must be the same length.");

// Increments a number or writes 0.
// Used for reading input.
static void inc_or_zero(uint8_t *value, bool increment);

void Io_input(Io *io, Input *input) {
    ControlScheme *ctrls = &io->controls;

    input->quit = WindowShouldClose();
    inc_or_zero(&input->left, IsKeyDown(ctrls->key_left));
    inc_or_zero(&input->right, IsKeyDown(ctrls->key_right));
    inc_or_zero(&input->hard_drop, IsKeyDown(ctrls->key_hard_drop));
    inc_or_zero(&input->soft_drop, IsKeyDown(ctrls->key_soft_drop));
    inc_or_zero(&input->rot_clockwise, IsKeyDown(ctrls->key_rot_clockwise));
    inc_or_zero(&input->rot_widdershins, IsKeyDown(ctrls->key_rot_widdershins));
    inc_or_zero(&input->pause, IsKeyDown(ctrls->key_pause));
}

bool read_controls(FILE *stream, ControlScheme *ctrls) {
    assert(stream != NULL);
    assert(ctrls != NULL);

    size_t inputs_set;
    for (inputs_set = 0; inputs_set < LEN(input_names); inputs_set++) {
        char buf[256];
        // Read a line. Break if file is done.
        if (fgets(buf, 256, stream) == NULL) {
            break;
        }

        // Parse the line into the input name (move-left, etc...) and keycode.
        char input_name[256];
        int keycode;
        if (sscanf(buf, "%s = %d\n", input_name, &keycode) != 2) {
            return false;
        }

        // Find out which input name we have.
        size_t input_id;
        for (input_id = 0; input_id < LEN(input_names); input_id++) {
            if (strcmp(input_name, input_names[input_id]) == 0) {
                break;
            }
        }
        if (input_id == LEN(input_names)) {
            return false;
        }

        // Set the relvant entry int the control scheme.
        *(int *) ((char *) ctrls + control_mem_offsets[input_id]) = keycode;
    }

    return inputs_set >= LEN(input_names);
}

void print_controls(FILE *stream, ControlScheme *ctrls) {
    for (size_t i = 0; i < LEN(input_names); i++) {
        fprintf(
            stream,
            "%s = %d\n",
            input_names[i],
            *(int *) ((char *) ctrls + control_mem_offsets[i])
        );
    }
}

void default_controls(ControlScheme *ctrls) {
    *ctrls = (ControlScheme) {
        .key_left = KEY_LEFT,
        .key_right = KEY_RIGHT,
        .key_hard_drop = KEY_SPACE,
        .key_soft_drop = KEY_DOWN,
        .key_rot_clockwise = KEY_UP,
        .key_rot_widdershins = KEY_X,
        .key_pause = KEY_ENTER,
    };
}

void joffe_controls(ControlScheme *ctrls) {
    *ctrls = (ControlScheme) {
        .key_left = KEY_LEFT,
        .key_right = KEY_RIGHT,
        .key_hard_drop = KEY_UP,
        .key_soft_drop = KEY_DOWN,
        .key_rot_clockwise = KEY_F,
        .key_rot_widdershins = KEY_D,
        .key_pause = KEY_ENTER,
    };
}

static void inc_or_zero(uint8_t *value, bool increment) {
    if (increment) {
        ++*value;
    }
    else {
        *value = 0;
    }
}
