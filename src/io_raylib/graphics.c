#include "internal.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <raylib.h>


// Call BeginDrawing() first and EndDrawing after these:
static void render_game(Tetris *);
static void render_cell(int8_t x, int8_t y, Color, int x0, int y0);
static void render_border(void); // the rectangle around the board.
static void render_mino(Mino, int x0, int y0, Color); // Render a mino relative to some position on the screen.
static void render_queue(MinoQueue *);
static void render_board(MinoShape **board); // renders the terrain of the board.

// Returns the proper color for rendering a mino.
static Color mino_color(MinoShape);

void Io_render_play(Io *io, Tetris *game) {
    (void) io;

    BeginDrawing();
    render_game(game);
    EndDrawing();
}

static void render_game(Tetris *game) {
    // TODO: Hold Piece.
    // TODO: Score.
    
    ClearBackground(DARKGRAY);
    render_border();
    render_board(game->board);
    render_mino(Tetris_ghost(game), BOARD_LEFT, BOARD_BOTTOM, (Color) {0, 0, 0, 128});
    render_mino(game->falling, BOARD_LEFT, BOARD_BOTTOM, mino_color(game->falling.shape));
    render_queue(&game->queue);
}

static void render_mino(Mino mino, int x0, int y0, Color color) {
    CoordsList coords = Mino_coords(mino);

    for (int8_t i = 0; i < 4; i++) {
        int8_t x = coords.coords[i].x;
        int8_t y = coords.coords[i].y;

        render_cell(x, y, color, x0, y0);
    }
}

static void render_cell(int8_t x, int8_t y, Color color, int x0, int y0) {
    if (y >= VISIBLE_HEIGHT) {
        return;
    }
    int board_x = x * CELL_WIDTH;
    int board_y = (y + 1) * CELL_WIDTH;

    int screen_x = x0 + board_x;
    int screen_y = y0 - board_y;

    DrawRectangle(screen_x, screen_y, CELL_WIDTH, CELL_WIDTH, color);
}

static void render_border(void) {
    Color color = BLACK;

    // Top.
    DrawLine(BOARD_LEFT, BOARD_TOP, BOARD_RIGHT + 1, BOARD_TOP, color);
    // Bottom.
    DrawLine(BOARD_LEFT, BOARD_BOTTOM, BOARD_RIGHT + 1, BOARD_BOTTOM + 1, color);
    // Left.
    DrawLine(BOARD_LEFT, BOARD_TOP, BOARD_LEFT, BOARD_BOTTOM + 1, color);
    // Right.
    DrawLine(BOARD_RIGHT + 1, BOARD_TOP, BOARD_RIGHT + 1, BOARD_BOTTOM + 1, color);
}

static void render_queue(MinoQueue *q) {
    // Just one piece for now.
    MinoShape next = MinoQueue_peek(q, 0);
    Mino mino = (Mino) {
        .origin = {0, 0},
        .rot = 0,
        .shape = next,
    };

    render_mino(mino, NEXT_MINO_LEFT, NEXT_MINO_BOTTOM, mino_color(mino.shape));
}

static void render_board(MinoShape **board) {
    for (int8_t y = 0; y < VISIBLE_HEIGHT; y++) {
        for (int8_t x = 0; x < BOARD_WIDTH; x++) {
            Color color = mino_color(board[y][x]);
            render_cell(x, y, color, BOARD_LEFT, BOARD_BOTTOM);
        }
    }
}

static Color mino_color(MinoShape shape) {
    switch (shape) {
        case MINO_I:
            return SKYBLUE;
        case MINO_T:
            return PURPLE;
        case MINO_O:
            return YELLOW;
        case MINO_S:
            return LIME;
        case MINO_Z:
            return RED;
        case MINO_J:
            return BLUE;
        case MINO_L:
            return ORANGE;
        case MINO_NONE:
            return BLANK;
        default:
            assert(false && "mino_color: `shape` must be a valid minoshape.");
            return BLACK;
    }
}
