#include "internal.h"

#include <stdio.h>
#include <stdlib.h>

#include <raylib.h>

Io *Io_init(int framerate) {
    Io *io = calloc(1, sizeof(*io));

    bool controls_set = false;
    FILE *controlfile = fopen("controls.txt", "r+");

    if (controlfile != NULL) {
        controls_set = read_controls(controlfile, &io->controls);
    }
    if (!controls_set) {
        controls_set = true;
        joffe_controls(&io->controls);
    }
    
    InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Tetric!");
    SetTargetFPS(framerate);

    fclose(controlfile);
    return io;
}

void Io_deinit(Io *io) {
    if (io == NULL) {
        return;
    }

    CloseWindow();

    free(io);
}
