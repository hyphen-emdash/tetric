#include "../io.h"

#include <stdio.h>

#define SCREEN_WIDTH    (640)
#define SCREEN_HEIGHT   (480)

#define CELL_WIDTH      (20)

#define BOARD_TOP       (40)
#define BOARD_BOTTOM    (440)
#define BOARD_LEFT      (SCREEN_WIDTH / 2 - BOARD_WIDTH * CELL_WIDTH / 2)
#define BOARD_RIGHT     (BOARD_LEFT + BOARD_WIDTH * CELL_WIDTH)

#define NEXT_MINO_LEFT      (BOARD_RIGHT + 40)
#define NEXT_MINO_TOP       (BOARD_TOP)
#define NEXT_MINO_BOTTOM    (NEXT_MINO_TOP + 6 * CELL_WIDTH)

typedef struct {
    int key_left;
    int key_right;
    int key_hard_drop;
    int key_soft_drop;
    int key_rot_clockwise;
    int key_rot_widdershins;
    int key_pause;
} ControlScheme;

struct iohandle {
    ControlScheme controls;
};

// Tries to read the control scheme from a configuration file
// (.tetric). Returns true on success or false on failure.
// If the file did not exist, does not try to create it.
bool read_controls(FILE *stream, ControlScheme *);

// Sets the controls to the default as established by the
// Tetris guidelines:
// https://tetris.fandom.com/wiki/Tetris_Guideline
void default_controls(ControlScheme *);

// My favourite control scheme.
void joffe_controls(ControlScheme *);

// Prints out a given control scheme to a file
// in a format that can be read later.
void print_controls(FILE *stream, ControlScheme *);
