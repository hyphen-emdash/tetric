#ifndef SCORE_H
#define SCORE_H

#include <stdbool.h>
#include <stdint.h>

typedef struct {
    uint32_t level;
    uint32_t lines;
    uint32_t points;
} Score;

// All the different ways to get points
// by locking in a piece.
typedef struct {
    int8_t combo_len;
    int8_t hard_drop_dist;
    int8_t soft_drop_dist;
    int8_t lines_cleared;
    bool back_to_back_tetris;
    bool t_spin; // TODO: mini_t_spin?
} LockPoints;

// Increment the score according to how
// a piece was locked in.
void Score_inc(Score *, LockPoints);

#endif // SCORE_H
