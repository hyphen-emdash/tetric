#include "minoqueue.h"

#include <assert.h>
#include <stdint.h>
#include <string.h>

// The noise function we'll be using.
static uint32_t squirrel3(uint32_t pos, uint32_t seed);

// Shuffles an array of mino shapes.
static void shuffle(size_t n, MinoShape *bag, uint32_t pos, uint32_t seed);

MinoShape MinoQueue_pull(MinoQueue *q) {
    MinoShape ret = MinoQueue_peek(q, 0);
    q->drawn++;
    return ret;
}

MinoShape MinoQueue_peek(const MinoQueue *q, uint32_t distance) {
    uint32_t t = q->drawn + distance;
    uint32_t n_shuffles = t / MINO_COUNT;
    uint32_t i = t % MINO_COUNT;

    MinoShape bag[MINO_COUNT];
    memcpy(bag, MINOSHAPE_ALL, sizeof(bag));

    shuffle(MINO_COUNT, bag, n_shuffles, q->seed);

    return bag[i];
}

static uint32_t squirrel3(uint32_t x, uint32_t seed) {
    // Lots of arithmetic overflow to come.

    x *= 0x68E31DA4;
    x += seed;
    x ^= x >> 8;
    x += 0xB5297A4D;
    x ^= x << 8;
    x *= 0x1B56C4E9;
    x ^= x >> 8;
    return x;
}

static void shuffle(size_t n, MinoShape *bag, uint32_t pos, uint32_t seed) {
    assert(bag != NULL);

    for (size_t i = 0; i < n; i++) {
        // TODO: Don't use modulo here--bad statistical properties.
        size_t swap_i = squirrel3(pos++, seed) % (n - i) + i;
        MinoShape tmp = bag[i];
        bag[i] = bag[swap_i];
        bag[swap_i] = tmp;
    }
}
