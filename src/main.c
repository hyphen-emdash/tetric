#include <assert.h>
#include <stdio.h>
#include <time.h>

#include "tetris.h"
#include "score.h"
#include "io.h"

#define CEIL_DIV(top, bottom) ((top / bottom) + (top % bottom != 0))

#define FPS (60)    // Frames per second.

Score play_game(Io *);

uint32_t move_rate(uint32_t framerate);
uint32_t fall_rate(uint32_t framerate, uint32_t level, bool soft_dropping);

int main() {
    int errcode = 0;

    Io *io = Io_init(FPS);
    if (io == NULL) {
        errcode = 1;
        goto end;
    }

    play_game(io);

end:
    Io_deinit(io);
    return errcode;
}

Score play_game(Io *io) {
    Tetris game;
    MinoShape board[BOARD_HEIGHT * BOARD_WIDTH];
    Tetris_init(&game, board, (uint32_t) time(NULL));

    Score score = (Score) {
        .level = 1,
        .points = 0,
        .lines = 0,
    };

    uint32_t frame = 0;
    Input input = {0};

    bool done = false;
    while (!done) {
        // Input.
        Io_input(io, &input);

        if (input.quit) {
            done = true;
        }
        if (input.left == 1 || (input.left > 10 && frame % move_rate(FPS) == 0)) {
            Tetris_move(&game, (Vec2) {-1, 0});
        }
        if (input.right == 1 || (input.right > 10 && frame % move_rate(FPS) == 0)) {
            Tetris_move(&game, (Vec2) {1, 0});
        }
        if (input.rot_clockwise == 1) {
            // TODO: check success
            Tetris_rot(&game, CLOCKWISE);
        }
        if (input.rot_widdershins == 1) {
            Tetris_rot(&game, WIDDERSHINS);
        }
        
        if (input.hard_drop == 1) {
            Tetris_hard_drop(&game);
        }
        
        // Update game.
        // TODO: check hard drop.
        if (frame % fall_rate(FPS, score.level, input.soft_drop) == 0) {
            bool fell = Tetris_move(&game, (Vec2) {0, -1});
            // TODO: ground timer seperate from fall timer.
            if (!fell) {
                int lock_status = Tetris_lock(&game);
                if (lock_status == LOCK_GAME_OVER) {
                    done = true;
                }
            }
        }
        
        // TODO: check this only on lock.
        int8_t lines[BOARD_HEIGHT];
        size_t n_lines = filled_lines(game.board, BOARD_HEIGHT, lines);

        if (n_lines > 0) {
            // TODO: graphics.
            clear_lines(game.board, n_lines, lines);
        }
        
        // Render output.
        Io_render_play(io, &game);
        frame++;
    }

    return score;
}

uint32_t move_rate(uint32_t framerate) {
    return CEIL_DIV(framerate, 15);
}

uint32_t fall_rate(uint32_t framerate, uint32_t level, bool soft_dropping) {
    // TODO: use level.
    // TODO: tune this.
    (void) level;
    uint32_t ret = CEIL_DIV(framerate, 2);
    if (soft_dropping) {
        // Perhaps we should add movement speed to falling speed?
        // Bear in mind we have speed^-1 here.
        ret = move_rate(framerate);
    }
    return ret;
}
