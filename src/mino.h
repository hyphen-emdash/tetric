#ifndef MINOSHAPE_H
#define MINOSHAPE_H

#include <stdint.h>
#include <stddef.h>

#include "geometry.h"

typedef int8_t MinoShape;
#define MINO_I      (0)
#define MINO_T      (1)
#define MINO_O      (2)
#define MINO_S      (3)
#define MINO_Z      (4)
#define MINO_J      (5)
#define MINO_L      (6)
#define MINO_COUNT  (7)
#define MINO_NONE   INT8_MIN

extern const MinoShape MINOSHAPE_ALL[MINO_COUNT];

typedef struct {
    Vec2 coords[4];
} CoordsList;

CoordsList MinoShape_offsets(MinoShape shape, uint8_t rot);

#define MAX_KICK_SHIFTS (5)

// Takes a mino, its current rotation, and desired change
// of rotation, and gives the list of "kicks" that one
// should try (in order) before deeming it impossible
// to rotate. This includes the trivial kick (0, 0).
// Returns the number of kicks written into the buffer.
size_t MinoShape_kick_shifts(
    MinoShape shape,
    Rotation starting_rot,
    Rotation delta_rot,
    Vec2 buf[static MAX_KICK_SHIFTS]
);

typedef struct {
    Vec2 origin;
    uint8_t rot; // rotation; in [0, 4)
    MinoShape shape;
} Mino;

// Creates a new mino, given the dimensions of the board it will be on.
// The squares themselves may be slightly higher than the board.
Mino Mino_spawn(MinoShape, int8_t height, int8_t width);

// Takes a mino and returns a list with the
// co-ordinates of each of its squares.
CoordsList Mino_coords(Mino);

#endif // MINOSHAPE_H
