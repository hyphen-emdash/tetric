#include "mino.h"

#include <assert.h>
#include <stdbool.h>
#include <string.h>

const MinoShape MINOSHAPE_ALL[MINO_COUNT] = {
    MINO_I,
    MINO_T,
    MINO_O,
    MINO_S,
    MINO_Z,
    MINO_J,
    MINO_L,
};

// TODO: just write in the co-ordinates directly instead of drawing them in strings.

static const char i_rotations[4][16];
static const char t_rotations[4][16];
static const char o_rotations[4][16];
static const char s_rotations[4][16];
static const char z_rotations[4][16];
static const char j_rotations[4][16];
static const char l_rotations[4][16];

#define N_KICKS_JLTSZ   (4)
#define N_KICKS_I       (4)
#define N_KICKS_O       (0)

// All the nontrivial kicks that one should
// attempt when rotating a falling mino.
// (The O piece does not meaningfully rotate.)
// # Indexing
// The convention is `kick_array[starting_rotation][kick_id]`
// when rotating clockwise.
// If you're rotating counter-clockwise, use
// `kick_array[ending_rotation][kick_id]` and negate all
// the numbers.
static const Vec2 jltsz_kicks[4][N_KICKS_JLTSZ];
static const Vec2 i_kicks[4][N_KICKS_I];

CoordsList MinoShape_offsets(MinoShape shape, uint8_t rot) {
    const char (*table)[4][16];

    switch (shape) {
        case MINO_I:
            table = &i_rotations;
            break;
        case MINO_T:
            table = &t_rotations;
            break;
        case MINO_O:
            table = &o_rotations;
            break;
        case MINO_S:
            table = &s_rotations;
            break;
        case MINO_Z:
            table = &z_rotations;
            break;
        case MINO_J:
            table = &j_rotations;
            break;
        case MINO_L:
            table = &l_rotations;
            break;
        default:
            assert(false && "MinoShape_offsets: `shape` must be an actual mino shape.");
            table = NULL;
            break;
    }

    assert(rot < 4 && "MinoShape_offsets: `rot` must be in [0, 4).");
    const char *drawing = (*table)[rot];

    CoordsList ret;
    memset(&ret, 0, sizeof(ret));
    int8_t i = 0;

    for (int8_t y = 0; y < 4; y++) {
        for (int8_t x = 0; x < 4; x++) {
            assert(i <= 4 && "MinoShape_offsets: internal drawing error: incorrect number of cells");

            char c = drawing[y * 4 + x];

            switch (c) {
                case '.':
                    break;
                case '#':
                    ret.coords[i++] = (Vec2) {
                        .x = x,
                        .y = 4 - y, // Otherwise everything would need to be drawn upside-down.
                    };
                    break;
                default:
                    assert(false && "MinoShape_offsets: internal drawing error: invalid character.");
                    break;
            }
        }
    }

    assert(i == 4 && "MinoShape_offsets: internal drawing error: incorrect number of cells");

    return ret;
}

size_t MinoShape_kick_shifts(
    MinoShape shape,
    Rotation starting_rot,
    Rotation delta_rot,
    Vec2 buf[static MAX_KICK_SHIFTS]
) {
    assert(delta_rot == CLOCKWISE || delta_rot == WIDDERSHINS);
    Rotation lookup_rot;
    if (delta_rot == CLOCKWISE) {
        lookup_rot = starting_rot;
    }
    else {
        lookup_rot = ROT_ADD(starting_rot, delta_rot);
    }

    Vec2 trivial = {0, 0};
    buf[0] = trivial;
    size_t kicks_written = 1;

    switch (shape) {
        case MINO_O:
            // There is nothing to write.
            kicks_written += N_KICKS_O;
            break;
        case MINO_I:
            memcpy(buf + kicks_written, i_kicks, sizeof(Vec2) * N_KICKS_I);
            kicks_written += N_KICKS_I;
            break;
        case MINO_J: case MINO_L:
        case MINO_T:
        case MINO_S: case MINO_Z:
            memcpy(buf + kicks_written, jltsz_kicks, sizeof(Vec2) * N_KICKS_JLTSZ);
            kicks_written += N_KICKS_JLTSZ;
            break;
        default:
            assert(false && "MinoShape_kick_offsets: invalid minoshape.");
    }

    if (delta_rot == WIDDERSHINS) {
        // Counter-clockwise rotations have mirror kicks.
        for (size_t i = 0; i < kicks_written; i++) {
            buf[i].x *= -1;
            buf[i].y *= -1;
        }
    }

    return kicks_written;
}

Mino Mino_spawn(MinoShape shape, int8_t height, int8_t width) {
    return (Mino) {
        .origin = {.x = width / 2 - 2, .y = height}, // TODO: get proper values here, based on the shape.
        .rot = 0,
        .shape = shape,
    };
}

CoordsList Mino_coords(Mino mino) {
    CoordsList ret = MinoShape_offsets(mino.shape, mino.rot);

    for (int8_t i = 0; i < 4; i++) {
        ret.coords[i].x += mino.origin.x;
        ret.coords[i].y += mino.origin.y;
    }

    return ret;
}

// Using the super rotation system.
// https://tetris.fandom.com/wiki/SRS?file=SRS-pieces.png
static const char i_rotations[4][16] = {
    "...."
    "####"
    "...."
    "....",

    "..#."
    "..#."
    "..#."
    "..#.",

    "...."
    "...."
    "####"
    "....",

    ".#.."
    ".#.."
    ".#.."
    ".#..",
};

static const char t_rotations[4][16] = {
    ".#.."
    "###."
    "...."
    "....",

    ".#.."
    ".##."
    ".#.."
    "....",

    "...."
    "###."
    ".#.."
    "....",

    ".#.."
    "##.."
    ".#.."
    "....",
};

static const char o_rotations[4][16] = {
    "...."
    ".##."
    ".##."
    "....",

    "...."
    ".##."
    ".##."
    "....",

    "...."
    ".##."
    ".##."
    "....",

    "...."
    ".##."
    ".##."
    "....",
};

static const char s_rotations[4][16] = {
    ".##."
    "##.."
    "...."
    "....",

    ".#.."
    ".##."
    "..#."
    "....",

    "...."
    ".##."
    "##.."
    "....",

    "#..."
    "##.."
    ".#.."
    "....",
};

static const char z_rotations[4][16] = {
    "##.."
    ".##."
    "...."
    "....",

    "..#."
    ".##."
    ".#.."
    "....",

    "...."
    "##.."
    ".##."
    "....",

    ".#.."
    "##.."
    "#..."
    "....",
};

static const char j_rotations[4][16] = {
    "#..."
    "###."
    "...."
    "....",

    ".##."
    ".#.."
    ".#.."
    "....",

    "...."
    "###."
    "..#."
    "....",

    ".#.."
    ".#.."
    "##.."
    "....",
};

static const char l_rotations[4][16] = {
    "..#."
    "###."
    "...."
    "....",

    ".#.."
    ".#.."
    ".##."
    "....",

    "...."
    "###."
    "#..."
    "....",

    "##.."
    ".#.."
    ".#.."
    "....",
};

static const Vec2 jltsz_kicks[4][N_KICKS_JLTSZ] = {
    // 0 -> 1
    {
        {-1,  0},
        {-1,  1},
        { 0, -2},
        {-1, -2},
    },
    // 1 -> 2
    {
        { 1,  0},
        { 1, -1},
        { 0,  2},
        { 1,  2},
    },
    // 2 -> 3
    {
        { 1,  0},
        { 1,  1},
        { 0, -2},
        { 1, -2},
    },
    // 3 -> 0
    {
        {-1,  0},
        {-1, -1},
        { 0,  2},
        {-1,  2},
    },
};

static const Vec2 i_kicks[4][N_KICKS_I] = {
    // 0 -> 1
    {
        {-2,  0},
        { 1,  0},
        {-2, -1},
        { 1,  2},
    },
    // 1 -> 2
    {
        {-1,  0},
        { 2,  0},
        {-1,  2},
        { 2, -1},
    },
    // 2 -> 3
    {
        { 2,  0},
        {-1,  0},
        { 2,  1},
        {-1, -2},
    },
    // 3 -> 0
    {
        { 1,  0},
        {-2,  0},
        { 1, -2},
        {-2,  1},
    },
};
