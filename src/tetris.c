#include "tetris.h"

#include <assert.h>
#include <string.h>

#include "geometry.h"

void Tetris_init(Tetris *game, MinoShape *board, uint32_t seed) {
    // Blank out the board.
    for (size_t i = 0; i < BOARD_HEIGHT * BOARD_WIDTH; i++) {
        board[i] = MINO_NONE;
    }

    // Initialise our board with pointers into `board`.
    for (int8_t y = 0; y < BOARD_HEIGHT; y++) {
        game->board[y] = board + y * BOARD_WIDTH;
    }

    game->queue = (MinoQueue) {
        .seed = seed,
        .drawn = 0,
    };

    MinoShape falling_shape = MinoQueue_pull(&game->queue);
    game->falling = Mino_spawn(falling_shape, VISIBLE_HEIGHT, BOARD_WIDTH);
}

bool Tetris_move(Tetris *game, Vec2 movement) {
    assert(game != NULL);
    assert(mino_fits(game->board, game->falling));

    Mino potential = game->falling;
    potential.origin.x += movement.x;
    potential.origin.y += movement.y;

    if (mino_fits(game->board, potential)) {
        game->falling = potential;
        return true;
    } else {
        return false;
    }
}

bool Tetris_rot(Tetris *game, Rotation delta_rot) {
    assert(game != NULL);
    assert(delta_rot == CLOCKWISE || delta_rot == WIDDERSHINS);

    Mino potential = game->falling;
    potential.rot = ROT_ADD(potential.rot, delta_rot);

    Vec2 kicks[MAX_KICK_SHIFTS];
    size_t max_kicks = MinoShape_kick_shifts(potential.shape, potential.rot, delta_rot, kicks);

    assert(max_kicks > 0);
    for (size_t i = 0; i < max_kicks; i++) {
        potential.origin.x = game->falling.origin.x + kicks[i].x;
        potential.origin.y = game->falling.origin.y + kicks[i].y;

        if (mino_fits(game->board, potential)) {
            game->falling = potential;
            return true;
        }
    }

    return false;
}

int Tetris_lock(Tetris *game) {
    assert(game != NULL);
    assert(mino_fits(game->board, game->falling));

    int status = LOCK_CONTINUE;
    CoordsList coords = Mino_coords(game->falling);
    for (int8_t i = 0; i < 4; i++) {
        int8_t x = coords.coords[i].x;
        int8_t y = coords.coords[i].y;

        assert(game->board[y][x] == MINO_NONE);
        game->board[y][x] = game->falling.shape;

        if (y >= VISIBLE_HEIGHT) {
            status = LOCK_GAME_OVER;
        }
    }

    MinoShape next_shape = MinoQueue_pull(&game->queue);
    game->falling = Mino_spawn(next_shape, VISIBLE_HEIGHT, BOARD_WIDTH);

    return status;
}

int8_t Tetris_hard_drop(Tetris *game) {

    Mino new = Tetris_ghost(game);
    int8_t dist = game->falling.origin.y - new.origin.y;
    assert(new.origin.x == game->falling.origin.x);
    game->falling = new;

    Tetris_lock(game);
    
    return dist;
}

Mino Tetris_ghost(Tetris *game) {
    assert(game != NULL);

    Mino ghost = game->falling;

    // Fall until we collide with something.
    // TODO: refactor this so we can use Tetris_move.
    while (mino_fits(game->board, ghost)) {
        ghost.origin.y--;
    }

    // Un-collide with it.
    ghost.origin.y++;
    return ghost;
}

bool mino_fits(MinoShape **board, Mino mino) {
    assert(board != NULL);
    for (size_t y = 0; y < BOARD_HEIGHT; y++) {
        assert(board[y] != NULL);
    }

    CoordsList mino_coords = Mino_coords(mino);

    for (int8_t i = 0; i < 4; i++) {
        int8_t x = mino_coords.coords[i].x;
        int8_t y = mino_coords.coords[i].y;

        if (x < 0 || y < 0 || x >= BOARD_WIDTH || y >= BOARD_HEIGHT || board[y][x] != MINO_NONE) {
            return false;
        }
    }

    return true;
}

size_t filled_lines(MinoShape **board, size_t bufsize, int8_t *buf) {
    size_t n = 0;
    for (int8_t y = 0; n < bufsize && y < BOARD_HEIGHT; y++) {
        bool complete = true;
        for (int8_t x = 0; complete && x < BOARD_WIDTH; x++) {
            if (board[y][x] == MINO_NONE) {
                complete = false;
            }
        }
        if (complete) {
            buf[n++] = y;
        }
    }
    return n;
}

void clear_lines(MinoShape **board, size_t n, int8_t *line_indices) {
    assert(n <= INT8_MAX);
    // We remove in descending order so we don't mess up the indexing scheme.
    for (int8_t i = (int8_t) n - 1; i >= 0; i--) {
        int8_t y = line_indices[i];
        assert(0 <= y && y < BOARD_HEIGHT);

        // Clear the line.
        MinoShape *cleared_row = board[y];
        for (int8_t x = 0; x < BOARD_WIDTH; x++) {
            cleared_row[x] = MINO_NONE;
        }

        // Everything above us drops down 1.
        // `board` is an array of pointers to rows, so we
        // move the pointers, not the row data.
        for (; y < BOARD_HEIGHT - 1; y++) {
            // Swap row y with the one above it.
            MinoShape *tmp = board[y];
            board[y] = board[y + 1];
            board[y + 1] = tmp;
        }
    }
}
