#ifndef MINOQUEUE_H
#define MINOQUEUE_H

#include "mino.h"

typedef struct {
    uint32_t seed;
    uint32_t drawn;
} MinoQueue;

// Pull a piece from a MinoQueue, moving it to the next one.
MinoShape MinoQueue_pull(MinoQueue *);

// Look into the future and see what piece will be drawn.
MinoShape MinoQueue_peek(const MinoQueue *, uint32_t distance);

#endif // MINOQUEUE_H
