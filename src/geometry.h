#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <stdint.h>

// (0, 0) is the bottom-left cell in the board.
// Increasing x goes right.
// Increasing y goes up.

typedef struct {
    int8_t x;
    int8_t y;
} Vec2;

// Rotation is represented by an integer in [0, 4)
// and performed by adding a number mod 4.
typedef uint8_t Rotation;
#define CLOCKWISE (1)
#define WIDDERSHINS (3)

#define ROT_ADD(a, b)   (((a) + (b)) % 4)

#endif // GEOMETRY_H
