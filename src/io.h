#ifndef IO_H
#define IO_H

#include "tetris.h"

typedef struct iohandle Io;

// Each field stores the number
// of times the given input was pressed.
typedef struct {
    uint8_t quit;
    uint8_t left;
    uint8_t right;
    uint8_t hard_drop;
    uint8_t soft_drop;
    uint8_t rot_clockwise;
    uint8_t rot_widdershins;
    uint8_t pause;
} Input;

// Creates an Io handle. Necessary for all Io operations.
Io *Io_init(int framerate);

// Deinitialises an Io handle.
// It is safe to pass NULL.
void Io_deinit(Io *);

// Writes the user's desired move.
// If a button is held, the value of the appropriate field
// is incremented.
// If a button is not down, 0 is written to the field.
void Io_input(Io *, Input *);

// Displays everything the user needs to see while they're
// in control.
void Io_render_play(Io *, Tetris *);

#endif // IO_H
