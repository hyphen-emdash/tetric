#ifndef TETRIS_H
#define TETRIS_H

#include <stdbool.h>
#include <stdint.h>

#include "mino.h"
#include "minoqueue.h"

#define BOARD_WIDTH     (10)
#define BOARD_HEIGHT    (40)
#define VISIBLE_HEIGHT  (20)

typedef struct {
    // Array of pointers to rows.
    MinoShape *board[BOARD_HEIGHT];

    MinoQueue queue;
    Mino falling;
    // TODO: hold piece.
} Tetris;

// Initialises a tetris game.
// The caller must supply the memory for the board:
// an array of MinoShapes with length BOARD_HEIGHT * BOARD_WIDTH
// This memory will be arbitrarily manipulated
// during the course of the game, but not freed.
void Tetris_init(Tetris *, MinoShape *board, uint32_t seed);

// Tries to move the falling piece by a give vector.
// Returns whether the piece moved or not (it does
// not move if its new position is obstructed by
// the floor, walls, or terrain.)
bool Tetris_move(Tetris *, Vec2 movement);

// Tries to rotate the piece.
// `delta_rot` should be 1 or -1.
// Returns true on success, false on failure
// (may fail due to obstruction.)
bool Tetris_rot(Tetris *, Rotation delta_rot);

#define LOCK_CONTINUE   (0)
#define LOCK_GAME_OVER  (-1)

// Locks the falling piece into the board, and gets
// a new one from the queue.
// Returns LOCK_CONTINUE under normal circumstances.
// If the new piece spawned collides with the board,
// returns LOCK_GAME_OVER.
// TODO: return the number of cleared lines, or a list
// of cleared lines?
int Tetris_lock(Tetris *);

// "Hard drop"s the falling piece:
// lowers it down to the bottom and locks
// it in place immediately.
// Returns the distance fallen.
int8_t Tetris_hard_drop(Tetris *);

// Returns a mino with the shape, rotation, and position
// of the "ghost piece" -- where the falling piece would
// land if hard dropped.
Mino Tetris_ghost(Tetris *);

// Returns whether a mino fits within a board (array of pointers to
// rows). It can fail if any cell of the mino is out of bounds or
// if any cell collides with the board's terrain.
bool mino_fits(MinoShape **board, Mino);

// Writes to `buf` the index of all the completed lines.
// Returns the number of indices written.
// Writes in ascending order.
size_t filled_lines(MinoShape **board, size_t bufsize, int8_t *buf);

// Clears the given lines and moves the lines above them down.
// The given array must be in ascending order.
void clear_lines(MinoShape **board, size_t n, int8_t *line_indices);

#endif // TETRIS_H
