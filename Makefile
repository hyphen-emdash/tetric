CC := clang

DEBUG := -Og -g3 -ggdb -fsanitize=address,undefined -fno-omit-frame-pointer
RELEASE := -Os -O2 -DNDEBUG

LDFLAGS := -lraylib
CFLAGS := -std=c11 -Wall -Wpedantic -Wextra

SRCS := $(shell find src/ -name *.c)
OBJS = $(SRCS:src/%.c=$(BUILD_DIR)/%.o)
INCS := $(shell find src/ -name *.h)

.PHONY: clean debug release tetric iolib

debug:
	$(eval CFLAGS="$(CFLAGS) $(DEBUG)")
	$(MAKE) tetric CFLAGS=$(CFLAGS) BUILD_DIR=build/debug

release:
	$(eval CFLAGS="$(CFLAGS) $(RELEASE)")
	$(MAKE) tetric CFLAGS=$(CFLAGS) BUILD_DIR=build/release

tetric: $(OBJS) dirs
	$(CC) $(CFLAGS) $(OBJS) -o $(BUILD_DIR)/$@ $(LDFLAGS)

$(OBJS): $(BUILD_DIR)/%.o: src/%.c $(INCS) dirs
	@mkdir -p $(BUILD_DIR)
	$(CC) $(CFLAGS) -c $< -o $@

dirs:
	mkdir -p $(BUILD_DIR)
	mkdir -p $(BUILD_DIR)/io_raylib

clean:
	rm -r build