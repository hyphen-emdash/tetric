# Tetric

This is a hobby project of mine. Feel free to download and play, make a PR or open a ticket; just understand that I don't make any guarantees.

## Installation

Make sure you have [raylib](https://www.raylib.com/) installed and just compile the source as you please. The Makefile assumes Linux. Use `make debug` or `make release` to compile. The executable will be in `build/debug` or `build/release`.
